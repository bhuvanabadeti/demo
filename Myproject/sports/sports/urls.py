
from django.conf.urls import url, include
from django.contrib import admin
from sportsapp import views


from django.conf import settings


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^home/', views.home),
    url(r'^login/', views.login),
    url(r'^index/', views.index),
    url(r'^search/', views.search),
    url('^api/v1/', include('social_django.urls', namespace='social')),

    ]

