import form as form
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User, auth
from django.urls import reverse
from social_core.pipeline.user import create_user
from django.contrib.auth.models import User
from .forms import userform



def home(request):

    return render(request, 'games/home.html')

def login(request):
    form1 = userform
    if request.method == "POST":
        form1 = userform(request.POST)
        if form1.is_valid():
            username = form1.cleaned_data['username']
            first_name = form1.cleaned_data['first_name']
            last_name = form1.cleaned_data['last_name']
            password = form1.cleaned_data['password']
            email = form1.cleaned_data['email']
            User.objects.create_user(username=username, first_name=first_name, last_name=last_name, password=password, email=email)
            return HttpResponseRedirect('/home/')
    else:
        form1 = userform( )
    return render(request, 'games/login.html', {'frm': form1})


def index(request):
    return render(request, 'games/index.html')


def search(request):
    return render(request, 'games/search.html')


def accounts(request):
    return render(request, 'games/login.html')


