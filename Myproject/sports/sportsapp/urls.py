
from django.conf.urls import url
from django.contrib import admin
from sportsapp import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^home/', views.home),
    url(r'^login/', views.login),
    url(r'^index/', views.index),
    url(r'^search/', views.search),



]
